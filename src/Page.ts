import * as Moment from 'moment';
import { ToastrService } from 'ngx-toastr';

export class Page{
    constructor(protected toastr: ToastrService){
        
    }
    getSimpleDate(date){
        return Moment(date).format("DD-MM-YYYY HH:mm:ss")
    }

    showSuccess(message, title = "") {
        this.toastr.success(message,title,{
            positionClass:'toast-top-center'
        });
    }
}