import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageUploadListComponent } from './page-upload-list.component';

describe('PageUploadListComponent', () => {
  let component: PageUploadListComponent;
  let fixture: ComponentFixture<PageUploadListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageUploadListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageUploadListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
