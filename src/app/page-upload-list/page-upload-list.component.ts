import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from '../api.service';
import { Page } from '../../Page';

@Component({
    selector: 'app-page-upload-list',
    templateUrl: './page-upload-list.component.html',
    styleUrls: ['./page-upload-list.component.css']
})
export class PageUploadListComponent extends Page implements OnInit {

    images = [];
    constructor(
        protected toastr: ToastrService,
        private apiService: ApiService
    ) {
        super(toastr);
    }

    ngOnInit() {
        this.apiService.getImages().then((result)=>{
            console.log("RECEIVED IMAGES",result.data);
            this.images = result.data;
        });
    }

    deleteImage(imageId){
        this.apiService.deleteImage(imageId).then(()=>{
            this.showSuccess("File deleted succesfully!");
            for(var i = 0; i < this.images.length;i++){
                let image = this.images[i];
                if(image.id == imageId){
                    this.images.splice(i,1);
                    return;
                }
            }
        });
    }

    

}
