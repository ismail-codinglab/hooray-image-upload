import { Routes } from "@angular/router";
import { PageUploadComponent } from "./page-upload/page-upload.component";
import { PageUploadListComponent } from "./page-upload-list/page-upload-list.component";
import { PageUploadDetailsComponent } from "./page-upload-details/page-upload-details.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";

export const appRoutes: Routes = [
    { path: '',                 redirectTo: 'upload-list', pathMatch: 'full' },
    { path: 'upload-list',     component: PageUploadListComponent},
    { path: 'upload-details/:id',  component: PageUploadDetailsComponent },
    { path: 'upload',          component: PageUploadComponent },
    { path: '**',               component: PageNotFoundComponent }
];
  