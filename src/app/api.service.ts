import { Injectable } from '@angular/core';
import feathers from '@feathersjs/feathers';
import rest from '@feathersjs/rest-client';
import * as io from "socket.io-client";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers } from '@angular/http';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    private _url: string = "http://54.37.68.178:3030";
    private _app;
    private _restClient;
    private _uploadService;
    constructor(public httpClient: HttpClient) {
        this._restClient = rest(this._url);
        this._app = feathers().configure(this._restClient.angularHttpClient(httpClient, { HttpHeaders }));
        this._uploadService = this._app.service('uploads');

    }

    sendImage(image) {
        return this._uploadService.create({
            filename: image.name,
            file: image.url
        })
    }

    getImages() {
        return this._uploadService.find();
    }

    getImage(id) {
        return this._uploadService.get(id);
    }

    deleteImage(imageId) {
        return this._uploadService.remove(imageId);
    }

    changeImage() {

    }
}
