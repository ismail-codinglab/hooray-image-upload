import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpEvent } from '@angular/common/http'
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from '../api.service';
import { Page } from '../../Page';

@Component({
    selector: 'app-page-upload',
    templateUrl: './page-upload.component.html',
    styleUrls: ['./page-upload.component.css']
})
export class PageUploadComponent extends Page implements OnInit {

    accept = '*'
    files: File[] = []
    progress: number
    url = 'http://localhost:3030/uploads'
    hasBaseDropZoneOver: boolean = false
    httpEmitter: Subscription
    httpEvent: HttpEvent<{}>
    lastFileAt: Date

    sendableFormData: FormData//populated via ngfFormData directive

    constructor(
        protected toastr: ToastrService,
        public HttpClient: HttpClient,
        private apiService: ApiService
        ) {
            super(toastr);
        }

    ngOnInit() {
    }
    cancel() {
        this.progress = 0
        if (this.httpEmitter) {
            console.log('cancelled')
            this.httpEmitter.unsubscribe()
        }
    }


    uploadFiles(files: File[]): Subscription {
        for(var i = 0; i < files.length;i++){
            this.uploadFile(files[i]);
        }
        this.files.length = 0;
        return null;
    }

    uploadFile(file: File){
        console.log("file to upload",file);
        const reader  = new FileReader();
        reader.readAsDataURL(file);
        // when encoded, upload
        reader.addEventListener('load',() => {
            this.apiService.sendImage({
                name:file.name,
                url:reader.result
            }).then((response) =>{
                    // success
                    this.showSuccess(`successful uploaded ${response.filename}`)
                });
        }, false);
    }

    getDate() {
        return new Date()
    }
}
