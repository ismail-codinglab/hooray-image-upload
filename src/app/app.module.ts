import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PageUploadListComponent } from './page-upload-list/page-upload-list.component';
import { PageUploadDetailsComponent } from './page-upload-details/page-upload-details.component';
import { PageUploadComponent } from './page-upload/page-upload.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { appRoutes } from './routes';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
//plugins
import { ngfModule } from "angular-file";
import { ToastrModule } from 'ngx-toastr';

@NgModule({
    declarations: [
        AppComponent,
        PageNotFoundComponent,
        PageUploadListComponent,
        PageUploadDetailsComponent,
        PageUploadComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(
            appRoutes,
            // { enableTracing: true } // <-- debugging purposes only
        ),
        HttpClientModule,
        BrowserAnimationsModule,

        //plugins
        ngfModule,
        ToastrModule.forRoot()
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
