import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from '../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Page } from '../../Page';

@Component({
    selector: 'app-page-upload-details',
    templateUrl: './page-upload-details.component.html',
    styleUrls: ['./page-upload-details.component.css']
})
export class PageUploadDetailsComponent extends Page implements OnInit {
    image = {};
    imageId;
    constructor(
        protected toastr: ToastrService,
        private apiService: ApiService,
        private route: ActivatedRoute,
        private router: Router
    ) {
        super(toastr);
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.imageId = params['id']; // --> Name must match wanted parameter
            this.apiService.getImage(this.imageId).then((result) => {
                console.log("RECEIVED IMAGES", result);
                this.image = result;
            });
        });
    }

    deleteImage(imageId){
        this.apiService.deleteImage(imageId).then(()=>{
            this.showSuccess("File deleted succesfully!");
            this.router.navigate(['/upload-list']);
        });
    }

}
