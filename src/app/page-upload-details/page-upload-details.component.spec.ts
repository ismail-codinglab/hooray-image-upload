import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageUploadDetailsComponent } from './page-upload-details.component';

describe('PageUploadDetailsComponent', () => {
  let component: PageUploadDetailsComponent;
  let fixture: ComponentFixture<PageUploadDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageUploadDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageUploadDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
